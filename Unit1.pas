unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UProjectEuler, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    MyProjectEuler : cProjectEuler;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  MyProjectEuler.ReportResults(Memo1);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  MyProjectEuler := cProjectEuler.Create;
end;

end.
