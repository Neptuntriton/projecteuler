unit UProjectEuler;

interface

uses
  Math,
  StdCtrls,
  SysUtils,
  UProjectEulerConstants;



type
  cProjectEuler = class
  private
    MyRefToMemo : TMemo;

    function Task1(Limit             : int64)              : int64;
    function Task2(Limit             : int64)              : int64;
    function Task3(Number            : int64)              : int64;
    function Task4(Digits            : integer)            : int64;
    function Task5(UpperBorder       : integer)            : int64;
    function Task6(Limit             : int64)              : int64;
    function Task7(Index             : integer)            : int64;
    function Task8(InputDigitStream  : String)             : int64;
    function Task9(SumOfTriplet      : integer)            : int64;
    function Task10(Limit            : int64)              : int64;
    function Task11(InputNumberField : TTask11NumberFiled) : int64;
    function Task12(Limit            : int64)              : int64;
    function Task13(InputNumberField : TTask13NumberField; Digits : integer): int64;

    function Task16(Exponent         : integer)            : int64;
    function Task34                                        : int64;
    function Task40                                        : int64;

    function IsPrim(Input : int64):boolean;
  public
    procedure ReportResults(AMemo : TMemo);
  end;

implementation

{ cProjectEuler }

function cProjectEuler.IsPrim(Input: int64): boolean;
var
  c : int64;
begin
  Result := true;
  if Input > 3 then begin
    c := 2;
    while c < round(Sqrt(Input)) + 2 do begin
      if (Input mod c) = 0 then begin
         Result := false;
         break;
      end;
      c := c + 1;
    end;
  end;
end;

procedure cProjectEuler.ReportResults(AMemo: TMemo);
var
  a : integer;
begin
  if AMemo <> nil then begin
    MyRefToMemo := AMemo;
    with AMemo.Lines do begin
      {
      Add('Task 1');
      Add('|-> Solution: ' + IntToStr(Task1(1000)));
      Add('Task 2');
      Add('|-> Solution: ' + IntToStr(Task2(4000000)));
      Add('Task 3');
      Add('|-> Solution: ' + IntToStr(Task3(600851475143)));
      Add('Task 4');
      Add('|-> Solution: ' + IntToStr(Task4(3)));
      }
      Add('Task 5');
      Add('|-> Solution for 10: ' + IntToStr(Task5(10)));
      Add('|-> Solution for 20: ' + IntToStr(Task5(20)));
      Add('|-> Solution for 30: ' + IntToStr(Task5(30)));

      {
      Add('Task 6');
      Add('|-> Solution: ' + IntToStr(Task6(100)));
      Add('Task 7');
      Add('|-> Solution: ' + IntToStr(Task7(10001)));
      Add('Task 8');
      Add('|-> Solution: ' + IntToStr(Task8(Task8DigitStream)));
      Add('Task 9');
      Add('|-> Solution: ' + IntToStr(Task9(1000)));
      Add('Task 10');
      Add('|-> Solution: ' + IntToStr(Task10(2000000)));

      Add('Task 11');
      Add('|-> Solution: ' + IntToStr(Task11(Task11NumberField)));
      }
      Add('Task 12');
      Add('|-> Solution for 5: ' + IntToStr(Task12(5)));
      Add('|-> Solution for 30: ' + IntToStr(Task12(30)));
      Add('|-> Solution for 500: ' + IntToStr(Task12(500)));
      Add('Task 13');
      Add('|-> Solution: ' + IntToStr(Task13(Task13NumberField, 10)));

      Add('Task 16');
      Add('|-> Solution for 15: ' + IntToStr(Task16(15)));
      Add('|-> Solution for 30x30: ' + IntToStr(Task16(30*30)));
      Add('|-> Solution for 1000: ' + IntToStr(Task16(1000)));

      Add('Task 34');
      Add('|-> Solution is: ' + IntToStr(Task34));

      Add('Task 40');
      Add('|-> Solution is: ' + IntToStr(Task40));


    end;
  end;
end;

// http://projecteuler.net/problem=1
function cProjectEuler.Task1(Limit: int64): int64;
var
  MultipleOfThree : int64;
  MultipleOfFive  : int64;
  Last, New       : Int64;
begin
  Result := 0;
  Last            := 1;
  MultipleOfThree := 0;
  MultipleOfFive  := 0;
  while (MultipleOfThree < Limit) or (MultipleOfFive < Limit) do begin
    if MultipleOfThree <= MultipleOfFive then begin
      MultipleOfThree := MultipleOfThree + 3;
      New             := MultipleOfThree;
    end else begin
      MultipleOfFive  := MultipleOfFive + 5;
      New             := MultipleOfFive;
    end;

    if (New <> Last) and (New < Limit) then begin
      Result := Result + New;
      Last   := New;
    end;
  end;
end;


// http://projecteuler.net/problem=2
function cProjectEuler.Task2(Limit: int64): int64;
var
  Minus2, Minus1, Fibo : int64;
begin
  Result := 2;
  Minus2 := 1;
  Minus1 := 2;
  Fibo   := 3;
  while Fibo < Limit do begin
    if (Fibo and $01 = 0) then begin
      Result := Result + Fibo;
    end;
    Fibo   := Minus2 + Minus1;
    Minus2 := Minus1;
    Minus1 := Fibo;
  end;
end;


// http://projecteuler.net/problem=3
// http://de.wikipedia.org/wiki/Faktorisierungsverfahren#Probedivision
function cProjectEuler.Task3(Number: int64): int64;
var
  a : int64;
  b : integer;

begin
  // Wie geht die Zerlegung �berhaupt
  Result := Number;
  a      := Round(Sqrt(Number)) + 1;
  while a > 1 do begin
    b := Number mod a;
    if (b = 0) and IsPrim(a) then begin
      Result := a;
      break;
    end;
    a := a - 1;
  end;
end;


// http://projecteuler.net/problem=4
function cProjectEuler.Task4(Digits: integer): int64;
var
  LastPalinDrome                       : int64;
  UpperBorderString, LowerBorderString : String;
  UpperBorder, LowerBorder, a, b       : integer;

  function IsPalindrome(Number : int64): boolean;
  var
    ALength, c : integer;
    AString    : string;
  begin
    Result  := true;
    AString := IntToStr(Number);
    ALength := length(AString);
    for c := 0 to ALength - 1 do if AString[c + 1] <> AString[ALength - c] then begin
      Result := false;
      break;
    end;
  end;
begin
  // Some Setup Stuff
  UpperBorderString := '';
  LowerBorderString := '1';
  for a := 1 to Digits do begin
    UpperBorderString := UpperBorderString + '9';
    if a > 1 then
      LowerBorderString := LowerBorderString + '0';
  end;
  LowerBorder := StrToInt(LowerBorderString);
  UpperBorder := StrToInt(UpperBorderString);

  // The Check beginning at the top of the mountain
  // |-> It's Bruteforce not so clever
  LastPalinDrome := 0;
  a := UpperBorder;
  while a >= LowerBorder do begin
    b := UpperBorder;
    while b >= LowerBorder do begin
      if IsPalindrome(a * b) and ((a * b) > LastPalinDrome) then begin
        LastPalinDrome := a * b;
      end;
      dec(b);
    end;
    dec(a);
  end;

  Result := LastPalinDrome;
end;


//http://projecteuler.net/problem=5
function cProjectEuler.Task5(UpperBorder: integer): int64;
var
  Prims, Counts, SubCounts : array of integer;
  a, b, ToCheck            : integer;



  procedure AddPrim(Number : integer);
  var
    d     : integer;
    found : boolean;
  begin
    found := false;
    for d := 0 to UpperBorder -1 do begin
      if Prims[d] = Number then begin
        found        := true;
        SubCounts[d] := SubCounts[d] + 1;
        break;
      end;
    end;
    if not found then begin
      d := 0;
      while Prims[d] <> 0 do inc(d); 
      Prims[d]     := Number;
      SubCounts[d] := 1;
    end;
  end;

  function pow(Base, Exponent : integer):integer;
  var
    i : integer;
  begin
    Result := 1;
    for i := 1 to Exponent do Result := Result * Base;
  end;
begin
  setlength(Prims,     UpperBorder);
  setlength(Counts,    UpperBorder);
  setlength(SubCounts, UpperBorder);

  for a := 0 to UpperBorder -1 do begin
    Prims[a]     := 0;
    Counts[a]    := 0;
    SubCounts[a] := 0;
  end;

  for a := UpperBorder downto 2 do begin
    for b := 0 to UpperBorder -1 do SubCounts[b] := 0;

    ToCheck := a;
    while ToCheck > 1 do begin
      b := ToCheck;
      while b > 1 do begin
        if (ToCheck mod b = 0) and IsPrim(b) then begin
          AddPrim(b);
          ToCheck := ToCheck div b;
          b := 1;
        end;
        dec(b);
      end;
    end;

    for b := 0 to UpperBorder -1 do Counts[b] := max(Counts[b], SubCounts[b]);
  end;

  Result := 1;
  for a := 0 to UpperBorder -1 do begin
    if Counts[a] > 0 then begin
      Result := Result * Pow(Prims[a], Counts[a]);
    end;
  end;
end;

// http://projecteuler.net/problem=6
function cProjectEuler.Task6(Limit: int64): int64;
var
  a            : integer;
  SumOfSquares : int64;
  SquareOfSums : Int64;
begin
  SumOfSquares := 0;
  SquareOfSums := 0;
  for a := 1 to Limit do begin
    SumOfSquares := SumOfSquares + a * a;
    SquareOfSums := SquareOfSums + a;
  end;
  SquareOfSums := SquareOfSums * SquareOfSums;
  Result       := SquareOfSums - SumOfSquares;
end;

//http://projecteuler.net/problem=7
function cProjectEuler.Task7(Index: integer): int64;
var
  a, ToCheck : integer;
begin
  a       := 1;
  ToCheck := 2;
  while a < Index do begin
    Inc(ToCheck);
    if IsPrim(ToCheck) then inc(a);
  end;
  Result := ToCheck;
end;

// http://projecteuler.net/problem=8
function cProjectEuler.Task8(InputDigitStream: String): int64;
var
  ALength, a  : integer;
  AProdukt    : int64;
  function Product(Input : string): int64;
  var
    b : integer;
  begin
    Result := 1;
    for b := 1 to 5 do Result := Result * StrToInt(Input[b]);
  end;
begin
  AProdukt := 0;
  ALength  := length(InputDigitStream);

  for a := 1 to ALength - 5 do begin
    AProdukt := max(AProdukt, Product(copy(InputDigitStream,a,5)));
  end;

  Result := AProdukt;
end;

// http://projecteuler.net/problem=9
function cProjectEuler.Task9(SumOfTriplet: integer): int64;
var
  a, b, c : integer;
  Found   : boolean;
begin
  Result := 0;
  Found  := false;
  for a := 1 to SumOfTriplet do begin
    for b := a + 1 To SumOfTriplet - a do begin
      for c := b + 1 to SumOfTriplet - a - b do begin
        if (a * a + b * b = c * c) and (a + b + c = SumOfTriplet) then begin
          Result := a * b * c;
          Found  := true;
          break;
        end;
      end;
      if Found then Break;
    end;
    if Found then Break;
  end;
end;


//http://projecteuler.net/problem=10
function cProjectEuler.Task10(Limit: int64): int64;
var
  a, Sum : int64;
begin
  a   := Limit;
  Sum := 0;
  while a > 1 do begin
    if IsPrim(a) then sum := sum + a;
    dec(a);
  end;
  Result := sum;
end;

function cProjectEuler.Task11(InputNumberField: TTask11NumberFiled): int64;
var
  x, y, NCols, NRows : integer;
  function ColProduct(XStart, YStart : integer) : int64;
  begin
    Result :=   InputNumberField[NCols * (YStart + 0) + XStart + 0]
              * InputNumberField[NCols * (YStart + 1) + XStart + 0]
              * InputNumberField[NCols * (YStart + 2) + XStart + 0]
              * InputNumberField[NCols * (YStart + 3) + XStart + 0];
  end;
  function RowProduct(XStart, YStart : integer) : int64;
  begin
    Result :=   InputNumberField[NCols * (YStart + 0) + XStart + 0]
              * InputNumberField[NCols * (YStart + 0) + XStart + 1]
              * InputNumberField[NCols * (YStart + 0) + XStart + 2]
              * InputNumberField[NCols * (YStart + 0) + XStart + 3];
  end;
  function DiagonalProduct(XStart, YStart : integer) : int64;
  begin
    Result :=   InputNumberField[NCols * (YStart + 0) + XStart + 0]
              * InputNumberField[NCols * (YStart + 1) + XStart + 1]
              * InputNumberField[NCols * (YStart + 2) + XStart + 2]
              * InputNumberField[NCols * (YStart + 3) + XStart + 3];
  end;
  function AntiDiagonalProduct(XStart, YStart : integer) : int64;
  begin
    Result :=   InputNumberField[NCols * (YStart + 0) + XStart - 0]
              * InputNumberField[NCols * (YStart + 1) + XStart - 1]
              * InputNumberField[NCols * (YStart + 2) + XStart - 2]
              * InputNumberField[NCols * (YStart + 3) + XStart - 3];
  end;
begin
  NCols := 20; NRows := 20; Result := 0;
  // Searching 4 in All Cols
  for x := 0 to NCols -1 do begin
    for y := 0 to NRows - 1 - 3 do begin
      Result := max(Result, ColProduct(x,y));
    end;
  end;
  // Searching 4 in All Rows
  for x := 0 to NCols - 1 - 3 do begin
    for y := 0 to NRows - 1 do begin
      Result := max(Result, RowProduct(x,y));
    end;
  end;
  // Searching 4 in All Diagonals
  for x := 0 to NCols - 1 - 3 do begin
    for y := 0 to NRows - 1 - 3 do begin
      Result := max(Result, DiagonalProduct(x,y));
    end;
  end;
  // Searching 4 in All Anti-Diagonals
  for x := 3 to NCols - 1 do begin
    for y := 0 to NRows - 1 - 3 do begin
      Result := max(Result, AntiDiagonalProduct(x,y));
    end;
  end;
end;

// http://www.matheboard.de/archive/9214/thread.html
function cProjectEuler.Task12(Limit: int64): int64;
var
  NumberCounter, a         : integer;
  DivisorCount, LineIndex  : integer;
  MaxDivisorCount          : integer;
  TriangleNumber           : int64;
  ToCheck                  : int64;
  Len                      : integer;
  MaxPrim                  : integer;
  Processed                : boolean;
  Prims                    : array of integer;
  Counts                   : array of integer;
  StartTime                : TDateTime;
begin
  StartTime       := Now;
  NumberCounter   := 0;
  TriangleNumber  := 0;
  DivisorCount    := 0;
  MaxDivisorCount := 0;
  MaxPrim         := 2;
  Len             := 1;
  setlength(Prims,  Len);
  setlength(Counts, Len);
  Prims[0] := 2;

  //MyRefToMemo.Lines.add('|-> MaxDivisorCount ' + IntToStr(MaxDivisorCount));
  //MyRefToMemo.Lines.add('|-> TriangleNumber ' + IntToStr(TriangleNumber));
  //LineIndex       := MyRefToMemo.Lines.Count - 2;

  while DivisorCount <= Limit do begin
    inc(NumberCounter);
    TriangleNumber := TriangleNumber + NumberCounter;

    // Do the factorisation
    for a := 0 to len -1 do Counts[a] := 0;
    ToCheck := TriangleNumber;
    while ToCheck > 1 do begin
      Processed := false;
      for a := 0 to Len -1 do begin
        if ToCheck mod Prims[a] = 0 then begin
          ToCheck := ToCheck div Prims[a];
          inc(Counts[a]);
          processed := true;
          break;
        end;
      end;
      if not Processed then begin
        // adding further prime numbers on the fly
        for a := MaxPrim + 1 to ToCheck do begin
          if IsPrim(a) then begin
            inc(len);
            setlength(Prims, Len);
            setlength(Counts, Len);
            Prims[Len - 1]  := a;
            Counts[Len - 1] := 0;
            MaxPrim         := a;
          end;
        end;
      end;
    end;

    // calculating the factorcount
    DivisorCount := 1;  // any number is diviable by 1
    for a := 0 to len - 1 do begin
      if Counts[a] > 0 then DivisorCount := DivisorCount * (Counts[a] + 1);
    end;


    MaxDivisorCount := max(DivisorCount, MaxDivisorCount);
    //MyRefToMemo.Lines.Strings[LineIndex]     := '|-> MaxDivisorCount ' + IntToStr(MaxDivisorCount);
    //MyRefToMemo.Lines.Strings[LineIndex + 1] := '|-> TriangleNumber ' + IntToStr(TriangleNumber);
  end;

  MyRefToMemo.Lines.add('|-> MaxDivisorCount; ' + IntToStr(MaxDivisorCount));
  MyRefToMemo.Lines.add('|-> Time: ' + FloatToStr(24 * 60 * 60 * 1000 * (now - StartTime)) + 'ms');

  Result := TriangleNumber;
end;

function cProjectEuler.Task13(InputNumberField: TTask13NumberField; Digits: integer): int64;
var
  a, b, ALength     : integer;
  Sum, RestAsString : String;
  rest              : integer;
begin
  ALength := length(InputNumberField[0]);
  Rest    := 0;
  sum     := '';
  for a := ALength downto 1 do begin
    for b := 0 to 99 do begin
      Rest := Rest + strtoint(InputNumberField[b][a]);
    end;
    RestAsString := IntToStr(rest);
    sum          := RestAsString[length(RestAsString)] + sum;
    Rest         := StrToInt(copy(RestAsString,1,length(RestAsString) - 1));
  end;

  RestAsString := IntToStr(rest);
  sum          := RestAsString + sum;

  MyRefToMemo.Lines.Add('|-> Sum: ' + sum);
  MyRefToMemo.Lines.Add('|-> Digits: ' + inttostr(length(sum)));
  Result := StrToInt64(Copy(sum,1,Digits));

end;


//http://projecteuler.net/problem=16
function cProjectEuler.Task16(Exponent: integer): int64;
var
  a, b     : integer;
  Number   : string;
  �bertrag : Byte;
  Digit    : Byte;
begin
  Number := '2';
  for a := 2 to Exponent do begin
    �bertrag := 0;
    for b := length(Number) downto 1 do begin
       Digit     := strtoint(Number[b]);
       Number[b] := inttostr((2 * Digit + �bertrag) mod 10)[1];
       �bertrag  := (2 * Digit + �bertrag) div 10
    end;
    if �bertrag > 0 then Number := IntToStr(�bertrag) + Number;
  end;
  result := 0;
  for a := 1 to length(Number) do begin
    result := result + strtoint(Number[a]);
  end;
end;



// http://projecteuler.net/problem=34
// 9! = 362880
// 7 x 9! ist 2540160 = z
// Eine 7 stellige Zahl aus 9en kann also nicht gr��er als z in der summer der fakult�ten der ziffern werden
// es reicht also wenn wir die zahlen bis 2540160 testen
function cProjectEuler.Task34: int64;
var
  a, z, b    : integer;
  Faks       : array[0..9] of integer;
  Sum, Summi : int64;
  Number     : string;
begin
  Faks[0] := 1;
  Faks[1] := 1;
  Faks[2] := 2;
  Faks[3] := 6;
  Faks[4] := 24;
  Faks[5] := 120;
  Faks[6] := 720;
  Faks[7] := 5040;
  Faks[8] := 40320;
  Faks[9] := 362880;

  Sum := 0;
  for a := 3 to 2540160 do begin
    Number := IntToStr(a);
    Summi  := 0;
    for b := 1 to length(Number) do begin
      Summi := Summi + Faks[StrToInt(Number[b])];
    end;
    if Summi = a then begin
      Sum := Sum + a;
      MyRefToMemo.Lines.Add(IntToStr(a));
    end;
  end;
  Result := Sum;
end;

//http://projecteuler.net/problem=40
function cProjectEuler.Task40: int64;
var
  a       : integer;
  Number  : string;
  Product : int64;
begin
  Product := 1;
  for a := 1 to 1000000 do begin
    Number := Number + IntToStr(a);
    if length(Number) > 1000000 then break;
  end;

  for a := 0 to 6 do begin

    Product := Product * strtoint(Number[round(Power(10,a))]);
  end;

  Result := Product;
end;


end.
